<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('peoples')->insert([
            [
                'name' => 'Katchen Mory',
                'position' => 'Vice President',
                'images' => 'team_pic2.jpg',
                'text' => 'Деловое письмо в котором выражается признательность сотруднику за его трудовые и иные результаты в развитии организации. Психологи утверждают, что для слухового восприятия человеку наиболее приятны такие слова, как его имя и спасибо. Так почему же этого не делать, если человек действительно заслуживает слов благодарности и похвалы. Благодарственное письмо является одним из вариантов деловых писем и играет роль составляющей части делового этикета.'
            ],
            [
                'name' => 'Tom Rensed',
                'position' => 'Chief Executive Officer',
                'images' => 'team_pic1.jpg',
                'text' => 'Деловое письмо в котором выражается признательность сотруднику за его трудовые и иные результаты в развитии организации. Психологи утверждают, что для слухового восприятия человеку наиболее приятны такие слова, как его имя и спасибо. Так почему же этого не делать, если человек действительно заслуживает слов благодарности и похвалы. Благодарственное письмо является одним из вариантов деловых писем и играет роль составляющей части делового этикета.',
            ],
            [
                'name' => 'Lancer Jack',
                'position' => 'Senior Manager',
                'images' => 'team_pic3.jpg',
                'text' => 'еловое письмо в котором выражается признательность сотруднику за его трудовые и иные результаты в развитии организации. Психологи утверждают, что для слухового восприятия человеку наиболее приятны такие слова, как его имя и спасибо. Так почему же этого не делать, если человек действительно заслуживает слов благодарности и похвалы. Благодарственное письмо является одним из вариантов деловых писем и играет роль составляющей части делового этикета.',
            ],
        ]);

        DB::table('portfolios')->insert([
            [
                'name' => 'Finance App',
                'images' => 'portfolio_pic2.jpg',
                'filter' => 'GPS',
            ],
            [
                'name' => 'Concept',
                'images' => 'portfolio_pic3.jpg',
                'filter' => 'design',
            ],
            [
                'name' => 'Shopping',
                'images' => 'portfolio_pic4.jpg',
                'filter' => 'android',
            ],
            [
                'name' => 'Managment',
                'images' => 'portfolio_pic5.jpg',
                'filter' => 'design',
            ],
            [
                'name' => 'iPhone',
                'images' => 'portfolio_pic6.jpg',
                'filter' => 'web',
            ],
            [
                'name' => 'Nexus',
                'images' => 'portfolio_pic7.jpg',
                'filter' => 'web',
            ],
            [
                'name' => 'Android',
                'images' => 'portfolio_pic8.jpg',
                'filter' => 'android',
            ],
        ]);

        DB::table('services')->insert([
            [
                'name' => 'Android',
                'text' => 'Какой-то текст надо тут было вставить но мне лень.',
                'icon' => 'fa-android',
            ],
            [
                'name' => 'Apple IOS',
                'text' => 'Какой-то текст надо тут было вставить но мне лень.',
                'icon' => 'fa-apple',
            ],
            [
                'name' => 'Design',
                'text' => 'Какой-то текст надо тут было вставить но мне лень.',
                'icon' => 'fa-dropbox',
            ],
            [
                'name' => 'Concept',
                'text' => 'Какой-то текст надо тут было вставить но мне лень.',
                'icon' => 'fa-html5',
            ],
            [
                'name' => 'User Research',
                'text' => 'Какой-то текст надо тут было вставить но мне лень.',
                'icon' => 'fa-slack',
            ],
            [
                'name' => 'User Experince',
                'text' => 'Какой-то текст надо тут было вставить но мне лень.',
                'icon' => 'fa-users',
            ],
        ]);

        DB::table('pages')->insert([
            [
                'name' => 'home',
                'alias' => 'home',
                'text' => 'Какой-то текст надо тут было вставить но мне лень.',
                'images' => 'main_device_image.jpg',
            ],
            [
                'name' => 'about us',
                'alias' => 'AboutUs',
                'text' => 'Какой-то текст надо тут было вставить но мне лень.',
                'images' => 'about-img.jpg',
            ],
        ]);


    }
}
